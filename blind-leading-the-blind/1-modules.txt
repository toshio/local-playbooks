https://anonbadger.wordpress.com/2014/12/15/tales-of-an-ansible-newb-ch-1-modules/

Tales of an Ansible Newb: Ch.1 - Modules

In my last post I used the ansible command line to run an arbitrary shell command on several remote systems.  Just being able to do that makes Ansible a useful tool in a sysadmin's toolchest.  However, that's just the tip of what Ansible can do.  Let's go one step further today and explore some Ansible modules.

The command from the last post looked like this:

<pre>
ansible '*' -i  'host1,host2' -a 'yum -y install tmux' -K --sudo
</pre>

That let me run an ad hoc command on a set of hosts.  You can run any command you want by specifying a different command line to <code>-a</code>:

<pre>
$ ansible '*' -i 'host1,' -a 'hostname'
host1 | success | rc=0 >>
host1.lan

$ ansible '*' -i 'fedora20-test,' -a 'grep fedora /etc/os-release'
fedora20-test | success | rc=0 >>
ID=fedora
CPE_NAME="cpe:/o:fedoraproject:fedora:20"
HOME_URL="https://fedoraproject.org/"
</pre>

Definitely useful.  But what if you need to combine several commands together?  For instance, let's say we want to run one command if we're talking to Fedora and a different one if we're talking to Ubuntu?  Well, we know how to use if-then-else in a shell script, maybe that will work here?  Let's give it a try!

<pre>
$ ansible '*' -i 'fedora20-test,ubuntu14' -a \
> 'if test -f /etc/fedora-release ; then echo "Fedora!" ; else echo "Ubuntu!" ; fi'
ubuntu14 | FAILED | rc=2 >>
[Errno 2] No such file or directory

fedora20-test | FAILED | rc=2 >>
[Errno 2] No such file or directory
</pre>

Nope that didn't work.  For a moment we might be puzzled why we get the error "No such file or directory" but then we realize that <code>if</code> is a shell builtin.  If ansible isn't creating a shell on the remote system then if won't be found.  So how can we do this?

Until now all of the commands I've given Ansible have been single programs and their command line parameters specified as a string to ansible's <code>-a</code> parameter.  But just what is the <code>-a</code>?  Ansible's
man page has this information for us:

<blockquote>
       -a 'ARGUMENTS', --args='ARGUMENTS'
                  The ARGUMENTS to pass to the module.
</blockquote>

So what's this module it's talking about?  Modules are small pieces of code that Ansible is able to run on a remote machine.  By default, if no module is specified, Ansible runs the <code>command</code> module.  The <code>command</code> module takes a single argument, a command line string which it then runs on the remote machine... with the important note that it directly invokes it so that we don't have to worry about a remote shell interpreting any special characters .  It does not pass it through the shell.  But if we do need to use shell constructs (shell variables, redirection, builtins, etc) we can use the shell module instead.  Woo hoo!  Let's try that:

<pre>
$ ansible '*' -i 'fedora20-test,ubuntu14' -m shell -a \
'if test -f /etc/fedora-release ; then echo "Fedora!" ; else echo "Ubuntu!"
; fi'
ubuntu14 | success | rc=0 >>
Ubuntu!

fedora20-test | success | rc=0 >>
Fedora!
</pre>

Excellent.  So now we know how to use the full range of shell features if we need to.  But I wonder what other modules Ansible ships with.

To find the answer to that, you can run:

<pre>
$ ansible-doc -l
</pre>

Which prints out a list of 240 or so modules which range from niche applications like managing specific brands of network devices to installing OS packages on various types of Linux distribution and copying files between the local system and remote hosts.  You can find information about any one of these by running <code>ansible-doc MODULE-NAME</code>.

So let's try out one of these modules.  Let's take our example of installing a package on several remote computers that we used last time and make it use the yum module instead of invoking yum via the command module.  <code>ansible-doc yum</code> tells us that the yum module takes a few arguments of which <code>name</code> is mandatory and specifies a package name.  <code>state</code> let's us tell the module what state we want the package to be in when ansible exits.  Let's give it a try:

<pre>
$ ansible '*' -i 'fedora20-test,' -m yum -a 'name=tmux,zsh state=latest' --sudo -K
sudo password: 
fedora20-test | success >> {
    "changed": true, 
    "msg": "", 
    "rc": 0, 
    "results": [
        "All packages providing tmux are up to date", 
        "Resolving Dependencies\n
	--> Running transaction check\n
	---> Package zsh.x86_64 0:5.0.7-1.fc20 will be updated\n
	---> Package zsh.x86_64 0:5.0.7-4.fc20 will be an update\n
	--> Finished Dependency Resolution\n\n
	Dependencies Resolved\n\n
	================================================================================\n
	Package       Arch             Version                 Repository         Size\n
	================================================================================\n
	Updating:\n zsh           x86_64           5.0.7-4.fc20            updates           2.5 M\n\n
	Transaction Summary\n
	================================================================================\n
	Upgrade  1 Package\n\nTotal download size: 2.5 M\nDownloading packages:\n
	Not downloading deltainfo for updates, MD is 2.9 M and rpms are 2.5 M\n
	Running transaction check\nRunning transaction test\nTransaction test succeeded\n
	Running transaction (shutdown inhibited)\n
	Updating   : zsh-5.0.7-4.fc20.x86_64                                      1/2 \n
	Cleanup    : zsh-5.0.7-1.fc20.x86_64                                      2/2 \n
	Verifying  : zsh-5.0.7-4.fc20.x86_64                                      1/2 \n
	Verifying  : zsh-5.0.7-1.fc20.x86_64                                      2/2 \n\n
	Updated:\n  zsh.x86_64 0:5.0.7-4.fc20                                                     \n\n
	Complete!\n"
    ]
}
</pre>

Yep, as we expect telling ansible to use the yum module and specifying that the arguments to the module are <code>name=tmux,zsh state=latest</code> makes sure that the tmux and zsh packages are installed and at their latest available versions.  But has using the yum module gained us anything?  It's about as long to type <code>-m yum -a 'name=tmux,zsh state=latest</code> as it is to type <code>-a 'yum install -y tmux zsh</code> and we already know the syntax for the latter.  Is there a difference?  For the yum module there isn't much difference.  The module and the single command do about the same thing.  But there are other modules that do more.  For instance, the git module.

Let's say that you have a web application in a git repository and you want to deploy it directly from git.  You need to clone the repository remotely and checkout a specific revision to the working tree because that's what should be running in production, not the current HEAD of the master branch.  You also need to checkout a few git submodules for projects that are hosted in different repositories.  If you did this via the shell module, you'd have to use several different calls to git:

<pre>
$ ansible '*' -i 'host1,' -a 'git clone http://git.example.com/project /var/www/webapp'
$ ansible '*' -i 'host1,' -m shell -a 'cd /var/www/webapp && git checkout fad30ad8'
$ ansible '*' -i 'host1,' -m shell -a 'cd /var/www/webapp && git submodule update --init'
</pre>

The git module encapsulates git's features in such a way that you can do all of this in one ansible call:

<pre>
$ ansible '*' -i 'host1,' -m git -a \
  'repo=http://git.example.com/project state=present version=fad30ad8 recursive=yes dest=/var/www/webapp'
</pre>

Kinda handy, right?  Where modules really start to shine, though, is when we stop trying to run everything in a single ansible command line and start using playbooks to perform multiple steps in a single run.  More on playbooks next time.
