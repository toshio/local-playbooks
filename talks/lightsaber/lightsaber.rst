.. transition:: dissolve
    :duration: 0.4


Build your own Ansible Lightsaber
=================================

* Copyright 2015, Toshio Kuratomi
* Licensed CC-BY-SA

What is Ansible?
================

.. image:: ansible-red-logo.jpg
    :scale: 50

What is Ansible?
================

.. container:: handout

    what is Ansible?  Ansible is an easy to setup tool to do things to remote
    machines over ssh.  You can use it to execute ad hoc tasks on one or many
    remote hosts.  You can combine a series of tasks into an ansible playbook
    and use it to orchestrate multistep tasks on remote systems.   And you can
    use it to manage the configuration and provisioning of all of the machines
    in your network.

    So let's take a look at some examples of what each of these mean.

.. container:: progressive

  * Ad hoc executor of remote tasks
  * Orchestration tool
  * Config management system

Example Ad hoc task
===================

.. container:: handout

    Ad hoc tasks are a place where people can start experimenting with
    Ansible.  Using Ansible in this way allows you to perform a single action
    on multiple boxes when you need to.

    For instance, let's say that it's 10:00 at night and you get a call from
    your boss.  Something's wrong with the web application that you're in
    charge of.  You take a look at the code, discover the problem, and work
    out a fix.  Since it's now Midnight, you don't worry about checking it in
    and going through the normal deployment channels -- you can do that in the
    morning.  Right now you just want to get the fix deployed to your two
    load balanced app servers so that you can get back to sleep.  Here's the
    Ad hoc ansible commands to deploy your fix:

.. code:: bash

    $ ansible -i 'webserver01,webserver02,' all -m copy \
        -a 'src=/home/badger/foo.py-fixed dest=/srv/webapp/foo.py' --sudo -K

    $ ansible -i 'webserver01,webserver02,' all -m service \
        -a 'name=httpd state=restarted' --forks 1 --sudo -K

Example Ad hoc task (output)
============================

.. transition:: cut
   :duration: 0.1

.. container:: handout

    The first command pushes the fixed file from your local machine out to the
    webservers.  When the task finishes on each host it prints out some
    information about whether it was successful and information about the new
    files.

.. code:: bash

    $ ansible -i 'webserver01,webserver02,webserver03,' all -m copy \
        -a 'src=/home/badger/foo.py-fixed dest=/srv/webapp/foo.py' --sudo -K
    SUDO password: 
    webserver01 | success >> {
        "changed": true,
        "checksum": "279d9035886d4c0427549863c4c2101e4a63e041",
        "dest": "/srv/webapp/foo.py",
        "gid": 0,
        "group": "root",
        "md5sum": "12f6bb1941df66b8f138a446d4e8670c",
        "mode": "0644",
        "owner": "root",
        "secontext": "system_u:object_r:var_t:s0",
        "size": 9,
        "src": "/tmp/ansible-tmp-1438966198.0-127408079214249/source",
        "state": "file",
        "uid": 0
    }

    webserver02 | success >> {
        "changed": true,
        "checksum": "279d9035886d4c0427549863c4c2101e4a63e041",
        "dest": "/srv/webapp/foo.py",
        "gid": 0,
        "group": "root",
        "md5sum": "12f6bb1941df66b8f138a446d4e8670c",
        "mode": "0644",
        "owner": "root",
        "size": 9,
        "src": "/tmp/ansible-tmp-1438966198.01-142246180532962/source",
        "state": "file",
        "uid": 0
    }

Example Ad hoc task (output2)
=============================

.. transition:: cut
   :duration: 0.1

.. container:: handout

    The second command restarts apache on the webservers.  Since the
    webservers are load balanced, I want to only do one of them at a time to
    try to eliminate end user visible service interruption.  Specifying
    --forks 1 does that.

.. code:: bash

    $ ansible -i 'webserver01,webserver02,' all -m service \
        -a 'name=httpd state=restarted' --forks 1 --sudo -K
    SUDO password: 
    webserver01 | success >> {
        "changed": true,
        "name": "httpd",
        "state": "started"
    }

    webserver02 | success >> {
        "changed": true,
        "name": "httpd",
        "state": "started"
    }

Example orchestration task
==========================

.. container:: handout

    On to orchestration!  Orchestration is a bit like shell scripting.  It's
    mainly used to automate tasks that you might have done manually at the
    shell before.  This class of use cases includes multi-step actions like
    deploying new versions of your application or bring up new virtual
    machines.  To handle these, we move on to writing plays and playbooks.

    In Ansible, plays are the way we put together multiple individual
    tasks to make a single logical action.  We then put groups of plays into
    a playbook to execute them.  Many playbooks contain just a single play but
    some tasks, like creating a new virtual machine, provisioning it, and then
    installing the software on it to make it into a specific type of server
    can contain multiple plays so that you can reuse the individual plays in
    other things.

    For our purposes, let's just demonstrate a simple playbook that performs
    a common sysadmin task.  Updating a host to the latest versions of
    a package:

.. code:: yaml

    ---
    - name: Update our hosts
      hosts: all
      tasks:
        - name: Display the packages to be updated
          yum:
            list: updates
          register: updates
        - debug:
            var: updates.results
        - name: Ask the user to confirm
          pause:
            prompt: >
              Verify the list of packages to update and then press
              [Enter] to continue or Ctrl-C a to Abort
        - name: Update packages
          sudo: True
          yum:
            state: latest
            name: "*"

Example orchestration task (output 1)
=====================================

.. container:: handout

   We can easily use this playbook to run against just the hosts we want by
   changing the inventory.  Here, we specify two hosts on the command line as
   the inventory.

.. code:: bash

    $ ansible-playbook -i 'fedora20,fedora21,' orchestration.yml --sudo -K
    SUDO Password:
    PLAY [Update our hosts] ******************************************************* 

    GATHERING FACTS *************************************************************** 
    ok: [localhost]
    ok: [fedora21]

    TASK: [Display the packages to be updated] ************************************ 
    ok: [fedora21]
    ok: [localhost]

Example orchestration task (output 2)
=====================================

.. container:: handout

   We can easily use this playbook to run against just the hosts we want by
   changing the inventory.  Here, we specify two hosts on the command line as
   the inventory.

.. code:: bash

    TASK: [debug var=updates.results] ********************************************* 
    ok: [localhost] => {
        "var": {
            "updates.results": [
                {
                    "arch": "x86_64",
                    "epoch": "0",
                    "name": "xine-lib",
                    "nevra": "0:xine-lib-1.2.6-8.fc22.x86_64",
                    "release": "8.fc22",
                    "repo": "rpmfusion-free-rawhide",
                    "version": "1.2.6",
                    "yumstate": "available"
                },
            ]
        }
    ok: [fedora21] => {
        "var": {
            "updates.results": [
                {
                    "arch": "x86_64",
                    "epoch": "0",
                    "name": "systemd-python3",
                    "nevra": "0:systemd-python3-216-25.fc21.x86_64",
                    "release": "25.fc21",
                    "repo": "updates",
                    "version": "216",
                    "yumstate": "available"
                },
            ]
        }

Example orchestration task (output 3)
=====================================

.. code:: bash

    TASK: [Ask the user to confirm] *********************************************** 
    [localhost, fedora21]
    Verify the list of packages to update and then press [Enter] to continue or Ctrl-C a to Abort:

    ok: [fedora21]

    TASK: [Update packages] *******************************************************
    changed: [fedora21]

    PLAY RECAP ******************************************************************** 
    fedora21                   : ok=5    changed=1    unreachable=0    failed=0

Example config management task
==============================

.. container:: handout

   Configuration management is very similar to orchestration.  Both use
   playbooks to define what they are going to do.  However, orchestration is
   about performing a task.  You might perform this task many times in a row
   you'd expect that each time you did so the task would run again.

   By contrast, configuration management is about defining the state of your
   machines.  The goal is to define what files, packages, etc need to be
   present to make the machine perform its function.  If you ran
   a configuration management playbook a second time, you'd expect that there
   would be no change on the second run.

   Let's see a simple example of installing apache for a webserver:

.. code:: yaml

   ---
   - hosts: webservers
     tasks:
        - name: Install apache and mod_ssl
          yum:
            name: httpd,mod_ssl
            state: present
        - name: drop our configuration files into place
          copy:
            src: httpd-local.conf
            dest: /etc/httpd/conf.d/httpd-local.conf
        - name: Ensure that apache is set to run
          service:
            name: httpd
            enabled: True
            state: started

What is Lightsaber
==================

.. container:: progressive

    * Created by Ralph Bean and Fedora Infrastructure Contributors
    * Collection of playbooks
    * Used for experimenting and maintaining their home systems

What do we get out of Lightsaber
================================

.. container:: progressive

    * Skeleton to help you organize your configs
    * Way to share roles that work with Fedora and EPEL

Let's do something!
===================

Customize a lightsaber now!

Fork the repo
==============

.. code:: bash

    $ git clone https://github.com/ralphbean/lightsaber

Create your own space to work
=============================

.. code:: bash

    $ cd lightsaber/playbooks
    $ mkdir badger
    $ cd badger
    $ mkdir {config,actions}


Create your own ansible.cfg
===========================

lightsaber/playbooks/badger/ansible.cfg:

.. code:: ini

    [defaults]
    hostfile=inventory
    roles_path=../../roles
    vault_password_file = ../../vault_pass

    [ssh_connection]
    pipelining=True
    ssh_args = -o ForwardAgent=yes

Organizing our playbooks
========================

.. container:: handout
    We can now put our previous playbooks into the new structure we've
    created.

.. code:: bash

    $ cd lightsaber/playbooks/badger/
    $ mv ~/{configure-apache.yml,httpd-local.conf} config/
    $ mv ~/update-hosts.yml actions/

Inventory your systems
======================

.. code:: ini

    localhost

    [vms]
    fedora21
    fedora22

    [baremetal]
    roan

    [fedora]
    fedora21
    fedora22
    roan

    [testhosts:children]
    vms


Populate some default variables
===============================

.. code:: bash

    $ cd lightsaber/badger/
    $ mkdir -p group_vars/
    $ vim group_vars/all

.. code:: yaml

    username: badger
    # Only needed if you use the irc-bouncer role
    ircnick: abadger1999
    twitternick: abadger1999
    # dict of users we want to provide stuff for (user config files)
    all_users:
      - user: badger
        homedir: /home/badger
    # Used by ssh role
    permit_root_login: false
    # Used by common role
    shell: /usr/bin/zsh

Roles
=====

.. container:: handout

    Roles help you to organize and share your configs.  They serve to
    encapsulate all the tasks and files needed to config a specific service
    They allow you to parameterize the config so that you can perform the same
    actions but with different values.

.. container:: progressive

    * Encapsulate a set of tasks
    * Parameterize an action

Use a role from lightsaber!
===========================

lightsaber/playbooks/badger/configs/irc-bouncer.yml:

.. code::

    ---
    - hosts: fedora21
      roles:
        - { role: irc }

Create your own role
====================

.. container:: handout

.. code:: bash

    $ cd lightsaber/roles
    $ ansible-galaxy init fedmsg
    $ cd fedmsg
    $ ls -F
    defaults/  files/  handlers/  meta/  README.md  tasks/  templates/  vars/
    $ rm -rf {handlers,meta,files,vars}

Define the tasks
================

roles/fedmsg/tasks/main.yml:

.. code:: yaml

    - yum:
        name: fedmsg,fedmsg-relay
        state: present
    - name: Configure fedmsg
      template:
        src: endpoints.py.j2
        dest: /etc/fedmsg.d/endpoints.py
    - service:
        name: fedmsg-relay
        enabled: True
        state: started

Create the Templates and Files
==============================

roles/fedmsg/templates/endpoints.py.j2:

.. code:: python

    config = dict(
        endpoints={
            "my_site": [
                "tcp://{{fedmsg_server}}:9940",
                #"tcp://stg.fedoraproject.org:9940",
            ],
        },
    )

Add default values for roles
============================

roles/fedmsg/defaults/main.yml

.. code:: yaml

    ---
    fedmsg_server: localhost

Final role directory tree
=========================

.. code::

    +-- defaults
    |   +-- main.yml
    +-- README.md
    +-- tasks
    |   +-- main.yml
    +-- templates
    +-- endpoints.py.j2


Invoke the role from a playbook
===============================

lightsaber/playbooks/badger/config/fedmsg-server.yml

.. code::

    ---
    - hosts: fedora22
      roles:
        - { role: fedmsg, fedmsg_server=fedora22 }

Credits
=======

.. cowsay:: Credits!

* Ansible written by Michael Dehaan and maintained by Ansible, Inc

  * https://github.com/ansible/ansible
  * http://docs.ansible.com/

* Lightsaber created by Ralph Bean and maintained by a motley crew of Fedora Contributors

  * https://github.com/ralphbean/lightsaber

* Presentation software, presentty written by James Blair

  * https://pypi.python.org/pypi/presentty

What about modules?
===================

